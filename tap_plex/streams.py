"""Stream type classes for tap-plex."""

from __future__ import annotations

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_plex.client import PlexStream


"""class ApInvoicesStream(PlexStream):

    name = "ap_invoices"
    path = "/accounting/v1/ap-invoices"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType, description="The invoice's system ID"),
        th.Property("invoiceNumber", th.StringType),
        th.Property("periodDisplayBegin", th.StringType),
        th.Property("periodDisplayEnd", th.StringType),
        th.Property("transactionDateBegin", th.DateTimeType),
        th.Property("transactionDateEnd", th.DateTimeType),
        th.Property("createdDateBegin", th.DateTimeType),
        th.Property("createdDateEnd", th.DateTimeType),
        th.Property("booked", th.BooleanType),
    ).to_dict()
"""

"""class ArInvoicesStream(PlexStream):
    name = "ar_invoices"
    path = "/accounting/v1/ar-invoices"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType, description="The invoice's system ID"),
        th.Property("invoiceNumber", th.StringType),
        th.Property("periodDisplayBegin", th.StringType),
        th.Property("periodDisplayEnd", th.StringType),
        th.Property("transactionDateBegin", th.DateTimeType),
        th.Property("transactionDateEnd", th.DateTimeType),
        th.Property("createdDateBegin", th.DateTimeType),
        th.Property("createdDateEnd", th.DateTimeType),
        th.Property("booked", th.BooleanType),
    ).to_dict()"""

class EmployeesStream(PlexStream):
    """Define custom stream."""

    name = "employees"
    path = "/mdm/v1/employees"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType, description="The user's system ID"),
        th.Property("badgeNumber", th.StringType),
        th.Property("userId", th.StringType),
        th.Property("firstName", th.StringType),
        th.Property("lastName", th.StringType),
        th.Property("supervisorId", th.UUIDType),
        th.Property("status", th.StringType),
        th.Property("postition", th.StringType),
        th.Property("shift", th.StringType),
        th.Property("building", th.StringType),
        th.Property("type", th.StringType),
        th.Property("department", th.StringType),
        th.Property("email", th.StringType),
    ).to_dict()

class InventoryReceiptsStream(PlexStream):
    """Define custom stream."""

    name = "inventory_receipts"
    path = "/inventory/v1-beta1/inventory-receiving/receipts"
    schema = th.PropertiesList(
        th.Property("externalReceiptCode", th.StringType),
        th.Property("partId", th.UUIDType),
    ).to_dict()

class InventoryShipmentsStream(PlexStream):
    """Define custom stream."""

    name = "inventory_shipments"
    path = "/inventory/v1/inventory-shipping/shipments"
    schema = th.PropertiesList(
        th.Property("externalShipmentCode", th.StringType),
        th.Property("shipmentStatus", th.StringType),
    ).to_dict()

class LotsStream(PlexStream):
    """Define custom stream."""

    name = "lots"
    path = "/inventory/v1-beta1/inventory-tracking/lots"
    schema = th.PropertiesList(
        th.Property("partId", th.UUIDType),
        th.Property("partNumber", th.StringType),
        th.Property("partRevision", th.StringType),
        th.Property("lotNumber", th.StringType),
        th.Property("beginManufacturedDateTime", th.DateTimeType),
        th.Property("endManufacturedDateTime", th.DateTimeType),
        th.Property("beginBestbyDateTime", th.DateTimeType),
        th.Property("endBestbyDateTime", th.DateTimeType),
        th.Property("beginSellByDateTime", th.DateTimeType),
        th.Property("endSellByDateTime", th.DateTimeType),
        th.Property("beginUseByDateTime", th.DateTimeType),
        th.Property("endUseByDateTime", th.DateTimeType),
    ).to_dict()

class PartsStream(PlexStream):
    """Define custom stream."""

    name = "parts"
    path = "/inventory/v1/inventory-definitions/parts"
    schema = th.PropertiesList(
        th.Property("partNumber", th.StringType),
        th.Property("partRevision", th.StringType),
        th.Property("partType", th.StringType),
        th.Property("partSource", th.StringType),
        th.Property("partStatus", th.StringType),
    ).to_dict()

class SupplyItemsStream(PlexStream):
    """Define custom stream."""

    name = "supply_items"
    path = "/inventory/v1/inventory-definitions/supply-items"
    schema = th.PropertiesList(
        th.Property("supplyItemNumber", th.StringType),
        th.Property("type", th.StringType),
        th.Property("category", th.StringType),
        th.Property("group", th.StringType),
    ).to_dict()

class JobStream(PlexStream):
    """Define custom stream."""

    name = "Jobs"
    path = "/scheduling/v1/jobs"
    schema = th.PropertiesList(
        th.Property("partId", th.UUIDType),
        th.Property("externalJobCode", th.StringType),
        th.Property("jobNumber", th.StringType),
        th.Property("jobStatuses", th.ArrayType(th.StringType)),
        th.Property("dueDateBegin", th.DateTimeType),
        th.Property("dueDateEnd", th.DateTimeType),
        th.Property("buildingId", th.UUIDType),
        th.Property("completedDateBegin", th.DateTimeType),
        th.Property("completedDateEnd", th.DateTimeType),
        th.Property("beginEarliestStartDateTime", th.DateTimeType),
        th.Property("endEarliestStartDateTime", th.DateTimeType),
        th.Property("priority", th.StringType),
    ).to_dict()

class PurchaseOrdersStream(PlexStream):
    """Define custom stream."""

    name = "purchase_orders"
    path = "/purchasing/v1/purchase-orders"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType),
        th.Property("pONumber", th.StringType),
        th.Property("status", th.StringType),
        th.Property("active", th.BooleanType),
        th.Property("receive", th.BooleanType),
        th.Property("type", th.StringType),
        th.Property("supplierId", th.UUIDType),
        th.Property("shipTo", th.StringType),
        th.Property("orderedDateBegin", th.DateTimeType),
        th.Property("orderedDateEnd", th.DateTimeType),
        th.Property("dueDateBegin", th.DateTimeType),
        th.Property("dueDateEnd", th.DateTimeType),
        th.Property("building", th.StringType),
        th.Property("department", th.StringType),
        th.Property("blanketOrder", th.BooleanType),
        th.Property("createdById", th.UUIDType),
        th.Property("createdDateBegin", th.DateTimeType),
        th.Property("createdDateEnd", th.DateTimeType),
        th.Property("modifiedById", th.UUIDType),
        th.Property("modifiedDateBegin", th.DateTimeType),
        th.Property("modifiedDateEnd", th.DateTimeType),
    ).to_dict()

class ReceiptsStream(PlexStream):
    """Define custom stream."""

    name = "receipts"
    path = "/purchasing/v1/receipts"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType),
        th.Property("pOLineItemId", th.UUIDType),
        th.Property("supplierId", th.UUIDType),
        th.Property("pOId", th.UUIDType),
        th.Property("partId", th.UUIDType),
        th.Property("itemId", th.UUIDType),
        th.Property("operationCode", th.StringType),
        th.Property("releaseId", th.UUIDType),
        th.Property("supplierShipperNumber", th.StringType),
        th.Property("receiveDateBegin", th.DateTimeType),
        th.Property("receiveDateEnd", th.DateTimeType),
        th.Property("receiveById", th.UUIDType),
    ).to_dict()

class SalesOrdersStream(PlexStream):
    """Define custom stream."""

    name = "sales_orders"
    path = "/sales/v1/orders"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType),
        th.Property("customerId", th.UUIDType),
        th.Property("orderNumber", th.StringType),
        th.Property("poNumber", th.StringType),
        th.Property("poNumberRevisionDateBegin", th.DateTimeType),
        th.Property("poNumberRevisionDateEnd", th.DateTimeType),
        th.Property("status", th.StringType),
        th.Property("type", th.StringType),
        th.Property("category", th.StringType),
        th.Property("orderDateBegin", th.DateTimeType),
        th.Property("orderDateEnd", th.DateTimeType),
        th.Property("expirationDateBegin", th.DateTimeType),
        th.Property("expirationDateEnd", th.DateTimeType),
        th.Property("terms", th.StringType),
        th.Property("incoTerms", th.StringType, description="International Commercial Terms"),
        th.Property("fob", th.StringType, description="Frieght on Board Items"),
        th.Property("freightTerms", th.StringType),
        th.Property("insideSalesId", th.UUIDType),
        th.Property("outsideSalesId", th.UUIDType),
        th.Property("createdById", th.UUIDType),
        th.Property("createdDateBegin", th.DateTimeType),
        th.Property("createdDateEnd", th.DateTimeType),
        th.Property("modifiedById", th.UUIDType),
        th.Property("modifiedDateBegin", th.DateTimeType),
        th.Property("modifiedDateEnd", th.DateTimeType),
    ).to_dict()

class AccountsStream(PlexStream):
    """Define custom stream."""

    name = "accounts"
    path = "/mdm/v1/chart-of-accounts"
    schema = th.PropertiesList(
        th.Property("id", th.UUIDType),
        th.Property("accountNumber", th.StringType),
        th.Property("name", th.StringType),
        th.Property("categoryType", th.StringType),
        th.Property("active", th.BooleanType),
    ).to_dict()